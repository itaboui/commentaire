<?php # mon controleur appelle mon model et ma vue 

#je suis dans le controleur et je require mon model
require("../model/userRepository.php");
$userId = getUserIdFromURI(); # $User prend la valeur du dernier element de mon url return $authorID

$response = getUser($userId); #  ma fonction me retourne $response qui est egal a laffichage d' un utilisateur 

require("../view/displayUserView.php"); # je require ma vue qui est en echo



function getUserIdFromURI(){          #fonction qui permet recuper de dernier element de mon url 1 2 3 4 5   
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;
                               
    return $authorId; 
}
