<?php 

function dbConnect(){
    try #je me connecte a ma base de donnée 
    {
        return new PDO('mysql:host=localhost;dbname=blogue;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}


function getUsers() #je crée ma fonction getUsers qui me permet de recupere les users de ma base de donée
{
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT u.id, u.email FROM user u WHERE 1') ;

    $response->execute(array());

    return $response;
}



function getUser($userId)  #

{
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT * FROM user  WHERE id = :userId ') ; # id est egal a userid

    $response->execute(array('userId' => $userId)); #user id recupere la valeur de notre id donné en parametre $userID

    return $response;
}


function createUser($email, $password)
{
   $bdd = dbConnect();

    $encryptedPassword = password_hash($password, PASSWORD_DEFAULT);
    $response = $bdd->prepare ('INSERT INTO `blogue`.user(`email`, `password`)
            VALUES (:email, :password)');

    $response->execute(array('email' => $email, 'password' => $encryptedPassword));


    return $response;

}

function updateUser($email, $userId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `user` SET 
                                `email`= :email                         
                                WHERE id = :userId') ;

    $response->execute(array(   'email' => $email,
                                'id' => $userId));

    return $response;
}

